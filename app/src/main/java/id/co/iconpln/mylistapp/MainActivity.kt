package id.co.iconpln.mylistapp

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object{

    }

    val listHero: ListView
        get() = lv_list_hero

    private var list: ArrayList<Hero> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loadListArrayAdapter()
        loadListBaseAdapter(this)
        setListItemClickListener(listHero)
    }

    private fun setListItemClickListener(listView: ListView) {
        listView.onItemClickListener =
            AdapterView.OnItemClickListener { adapterView, _, index, _ ->
                val hero = adapterView?.getItemAtPosition(index) as Hero
                Toast.makeText(this@MainActivity, "${hero.name}", Toast.LENGTH_SHORT).show()
                showDetailHero(list[index])
            }
    }

    private fun showDetailHero(hero: Hero) {
        val detailHeroIntent = Intent(this, DetailHeroActivity::class.java)
        detailHeroIntent.putExtra(DetailHeroActivity.EXTRA_HERO, hero)
        startActivity(detailHeroIntent)
    }


    fun loadListArrayAdapter() {
        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, getDataHero())
        listHero.adapter = adapter

    }

    private fun loadListBaseAdapter(context: Context) {
        list.addAll(HeroesData.listDataHero)

        val baseAdapter = ListViewHeroAdapter(context, list)
        listHero.adapter = baseAdapter
    }

    fun getDataHero(): Array<String> {
        val hero = arrayOf(
            "Cut Nyak Dien",
            "Ki Hajar Dewantoro",
            "Moh Yamin",
            "Pattimura",
            "R.A Kartini",
            "Soekarno"
        )
        return hero
    }
}
